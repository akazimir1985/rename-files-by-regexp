(ns rename-files.core
	(:require [me.raynes.fs    :as fs])  	
  (:gen-class))

(defn current-dir
  "Returns the directory that was obtained or sets the current directory by default"
  [& user-path]
  (let [dir (first user-path)]
    (cond
      (nil? dir) (.getPath fs/*cwd*)
      (not (fs/directory? dir)) (println (str user-path " is not a directory"))
      (not (fs/writeable? dir)) (println (str user-path " is not writeblle"))
      :else dir)))

(defn current-regex
  "Returns the java.util.regex.Pattern that was obtained or prints an error message"
  [user-reg]
  (cond
    (instance? java.util.regex.Pattern user-reg) user-reg
    :else (println (str user-reg " is not a java.util.regex.Pattern"))))

(defn new-name
  "Returns new name of file which will be obtained by rest of matching regex"
  [matching-coll]
  (reduce str (rest matching-coll)))

(defn rename-files 
  "Renames files by regex"
  [current-regx current-path]
  (let [begin-path current-path
        begin-regex current-regx]
    (map (fn [] 
            (if (re-matches begin-regex (fs/name %))
              (fs/rename % (new-name (re-matches begin-regex (.getName %))))))
      (fs/list-dir begin-path))))

(defn copy-rename
  "Copies and renames files"
  [user-regex user-path]
  (let [current-reg (current-regex user-regex)
        current-path (current-dir user-path)
        name-temp "renamed"
        temp-dir (str current-path (java.io.File/separator) name-temp)]
    (if (and current-reg current-path) 
      (do        
        (fs/copy-dir current-path temp-dir)
        (fs/delete-dir (str temp-dir (java.io.File/separator) name-temp))
        (fs/with-cwd temp-dir 
          (do 
            (println (str fs/*cwd* ":" current-reg ":" temp-dir))
            (println (rename-files current-reg temp-dir)))))
      (println "Stoped with error"))))

(defn -main
  "Copies in a temp directory and renames files"
  [user-regex & args]
  (copy-rename (re-pattern user-regex) (first args)))



(comment

(defn filter-files 
	[regexp filt-seq]
	(let [finish-seq []]
		(println regexp)
		(reduce #(if-not (nil? (re-matches regexp %2)) (conj %1 (first %2)) %1) [] filt-seq)))
(def all-files (filter-files #"(.*)supersized(.*)" (rename-files directory)))




(def directory (clojure.java.io/file "/home/cerber/Документы/Clj/clojure/rename-images/src/rename_images/image"))

(defn rename-files [d]
  (let [begin-path d
  	    i 0]
  	(map #(if (re-matches #"(.*)supersized(.*)" (.getName %)) (.renameTo (str "XYN" (inc i)) %)) (.listFiles begin-path))))

(map #(if (re-matches #"(.*)supersized(.*)" (.getName %)) (.renameTo % (File. (str "XYN" (inc 0))))) (.listFiles directory))

(def all-files (fs/list-dir directory))

(defn current-dir 
	[directory]
	(with-mutable-cwd (chdir directory)))

(def files (file-seq directory))
(take 10 files)

(ns my-file-utils
  (:import java.io.File))

(defn rename-files [d]
  (println "Files in " (.getName d))
  (doseq [f (.listFiles d)]
    (if (.isDirectory f)
      (print "d ")
      (print "- "))
    (println (.getName f))))

(defn filter-files 
	[regexp filt-seq]
	(let [finish-seq []]
		(reduce #(conj %1 %2) [] filt-seq)))

)
